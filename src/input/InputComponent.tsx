import React, { ReactElement } from 'react';

type InputProps  = {
    value: string,
    fieldName: string,
    children?: ReactElement
    onChange?: (e: any) => void
};

export const InputComponent = ({ value, fieldName, onChange, children }: InputProps): ReactElement => {
    return (
    <input type="text" value={value} name={fieldName} onChange={onChange}>{children}</input>
    )
}