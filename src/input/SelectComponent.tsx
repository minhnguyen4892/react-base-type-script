import React, {ReactElement} from 'react';
import { GenderItem } from './inputContainer';

type SelectProps = {
    value: string,
    fieldName: string,
    options: GenderItem[],
    onChange: (e: any) => void
}

export const SelectComponent = ({value, fieldName, options, onChange}: SelectProps) : ReactElement => {
    return (
        <select name={fieldName} id={fieldName} value={value} onChange={onChange}>
            {options.map((item, index) => (
                <option key={index} value={item.name}>{item.name}</option>
            ))}
        </select>
    )
}