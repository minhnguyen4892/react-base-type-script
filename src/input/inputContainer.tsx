import React from 'react';
import {InputComponent} from './InputComponent';
import {SelectComponent} from './SelectComponent';
import {TextAreaComponent} from './TextAreaComponent';

interface InputProps {

};

interface InputState {
    name: string,
    description?: string,
    gender: string,
    genderData: GenderItem[]
};

export interface GenderItem {
    name: string
};

export default class InputContainer extends React.Component<InputProps, InputState>  {
 constructor(props: InputProps) {
    super(props);
    this.state = {
        name: '',
        description: '',
        gender: '',
        genderData: [{
            name: 'male'
        }, {
            name: 'female'
        }]
    }
 }

 handleOnChange = (e: any): void => {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value } as InputState);
 }
 render() {
     const { name, gender, genderData, description } = this.state;
     console.log('state:', this.state)
     return (
         <>
         <InputComponent value={name} fieldName={"name"} onChange={this.handleOnChange} />
         <SelectComponent value={gender} fieldName={"gender"} options={genderData} onChange={this.handleOnChange} />
         <TextAreaComponent value={description} fieldName={"description"} onChange={this.handleOnChange} />
        </>
     )
 }

}