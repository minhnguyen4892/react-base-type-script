import React, { ReactElement } from 'react';
type TextAreaProps = {
    value?: string,
    fieldName: string,
    onChange: (e: any) => void
};

export const TextAreaComponent = ({value, fieldName, onChange}: TextAreaProps): ReactElement => {
    return (
        <textarea name={fieldName} id={fieldName} cols={30} rows={10} value={value} onChange={onChange}></textarea>
    )
}