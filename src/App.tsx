import React from 'react';
import logo from './logo.svg';
import './App.css';
import Counter from './Counter';
import InputContainer from './input/inputContainer';
import {PostListScription} from './hoc/PostComponent';
import {CommentListScription} from './hoc/CommentComponent';

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <Counter/>
      <InputContainer />
      <PostListScription />
      <CommentListScription />
    </div>
  );
}

export default App;
