import React from 'react';
import * as DataSource from './DataSource';

const HOCScription = (
    WrappedComponent,
    selectData
) => {
    return class extends React.Component {
        constructor (props) {
            super(props);
            this.state = {
                data: []
            }
        }
        componentDidMount () {
            console.log('selectData: ', selectData(DataSource))
            this.setState({
                data: selectData(DataSource)
            })
        }

        componentWillUnmount() {

        }

        render () {
            return (
                <WrappedComponent data={this.state.data} {...this.props} />
            )
        }
    }
};

export default HOCScription;