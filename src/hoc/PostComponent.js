import React from 'react';
import HOCScription from './HOCScription';
import * as DataSource from './DataSource';

const PostList = (props) => {
    return (
        <>
        <div>Post List Page</div>
        {
            props.data.map((item, index) => (
                <div key={index}>{item.id}-{item.title}</div>
            ))
        }
        </>
    )
}

export const PostListScription = HOCScription(
    PostList,
    (DataSource) => DataSource.getPosts()
)