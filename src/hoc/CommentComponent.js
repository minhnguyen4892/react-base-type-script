import React from 'react';
import * as DataSource from './DataSource';
import HOCScription from './HOCScription';

const CommentList = (props) => {
    return (
        <>
        <div>Comment List Page</div>
        {
            props.data.map((item, index) => (
                <div key={index}>{item.id}-{item.title}</div>
            ))
        }
        </>
    )
} 

export const CommentListScription = HOCScription(
    CommentList,
    (DataSource) => DataSource.getComments()
)