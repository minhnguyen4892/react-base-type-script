const getPosts =  () => ([
    { id: 1, title: 'post 1'},
    { id: 2, title: 'post 2'}
]);

const getComments = () => ([
    { id: 1, title: 'comment 1'},
    { id: 2, title: 'comment 2'}
])

export {
    getPosts,
    getComments
}