import * as React from 'react';

import Count from './Count';

interface Props {

};

interface State {
    count: number
};

export default class Counter extends React.PureComponent<Props, State> {
    state: State = {
        count: 1
    }

    incrementCount = () => {
        this.setState({ count: this.state.count + 1});
    };

    decrementCount = () => {
        this.state.count > 0  && this.setState({ count: this.state.count - 1});
    };

    render () {
        return (
            <>
                <Count count={this.state.count} />
                <button onClick={this.incrementCount}>Increment</button>
                <button onClick={this.decrementCount}>Decrement</button>
            </>
        )
    }
};